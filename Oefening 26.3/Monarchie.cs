﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_26._3
{
    internal class Monarchie : Land
    {
        private string _koning;

        public Monarchie() : base(){ }
        public Monarchie(string naam, string hoofdstad, string koning) : base(naam, hoofdstad) 
        {
            Koning = koning;
        }

        public string Koning { get { return _koning; } set { _koning = value; } }

        public override string ToString()
        {
            return base.ToString() + " (Koning(in) " + Koning + ")";
        }
    }
}
