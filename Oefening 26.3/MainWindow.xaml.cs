﻿using System.Windows;

namespace Oefening_26._3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            rdoMonarchie.IsChecked = true;
        }

        List<Land> lijstLanden = new List<Land>();

        private void btnLandToevoegen_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bool dezeBool = true;
                if (!(txtHoofdstad.Text.Any(char.IsDigit) || txtStaatshoofd.Text.Any(char.IsDigit) || txtLand.Text.Any(char.IsDigit)))
                {
                    if (!(string.IsNullOrEmpty(txtHoofdstad.Text) && string.IsNullOrEmpty(txtStaatshoofd.Text) && string.IsNullOrEmpty(txtLand.Text)))
                    {
                        if (rdoMonarchie.IsChecked == true) 
                        {
                            Monarchie dezeMonarchie = new Monarchie(txtLand.Text, txtHoofdstad.Text, txtStaatshoofd.Text);
                            foreach (Monarchie m in lijstLanden)
                            {
                                if (m.Naam.ToUpper().Contains(txtLand.Text.ToUpper()))
                                {
                                    dezeBool = false;
                                    break;
                                }
                            }
                            if (dezeBool == true)
                            {
                                lijstLanden.Add(dezeMonarchie);
                                txtResultaat.Text += dezeMonarchie.ToString() + Environment.NewLine;
                                TXTLeegmaken();
                            }
                            else
                            {
                                MessageBox.Show("Dit land werd reeds toegevoegd. Gelieve een ander land te kiezen.", "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                                TXTLeegmaken();
                            }
                        }
                        else if (rdoRepubliek.IsChecked == true)
                        {
                            Republiek dezeRepubliek = new Republiek(txtLand.Text, txtHoofdstad.Text, txtStaatshoofd.Text);
                            foreach (Republiek r in lijstLanden)
                            {
                                if (r.Naam.ToUpper().Contains(txtLand.Text.ToUpper()))
                                {
                                    dezeBool = false;
                                    break;
                                }
                            }
                            if (dezeBool == true)
                            {
                                lijstLanden.Add(dezeRepubliek);
                                txtResultaat.Text += dezeRepubliek.ToString() + Environment.NewLine;
                                TXTLeegmaken();
                            }
                            else
                            {
                                MessageBox.Show("Dit land werd reeds toegevoegd. Gelieve een ander land te kiezen.", "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                                TXTLeegmaken();
                            }
                        }
                        else if(rdoOverige.IsChecked == true)
                        {
                            Land ditLand = new Land(txtLand.Text, txtHoofdstad.Text);
                            foreach (Land l in lijstLanden)
                            {
                                if (l.Naam.ToUpper().Contains(txtLand.Text.ToUpper()))
                                {
                                    dezeBool = false;
                                    break;
                                }
                            }
                            if (dezeBool == true)
                            {
                                lijstLanden.Add(ditLand);
                                txtResultaat.Text += ditLand.ToString() + " (Staatshoofd " + txtStaatshoofd.Text + ")" + Environment.NewLine;
                                TXTLeegmaken();
                            }
                            else
                            {
                                MessageBox.Show("Dit land werd reeds toegevoegd. Gelieve een ander land te kiezen.", "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                                TXTLeegmaken();
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Gelieve alle velden in te vullen.", "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    MessageBox.Show("Gelieve geen getallen in te vullen.", "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
            catch
            {
                MessageBox.Show("Er is een fout opgedtreden bij het toevoegen van het land.", "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }
        public void TXTLeegmaken()
        {
            txtStaatshoofd.Text = string.Empty;
            txtHoofdstad.Text = string.Empty;
            txtLand.Text = string.Empty;
        }
    }
}