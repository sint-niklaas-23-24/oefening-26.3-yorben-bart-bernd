﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_26._3
{
    internal class Republiek : Land
    {
        private string _president;

        public Republiek() : base() { }
        public Republiek(string naam, string hoofdstad, string president) : base(naam, hoofdstad) 
        { 
            President = president;
        }

        public string President { get { return _president; } set { _president = value; } }

        public override string ToString()
        {
            return base.ToString() + " (President " + President + ")";
        }
    }
}
