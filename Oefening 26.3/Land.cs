﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_26._3
{
    internal class Land
    {
        private string _hoofdstad;
        private string _naam;

        public Land() 
        {
        }
        public Land(string naam, string hoofdstad)
        {
            Hoofdstad = hoofdstad;
            Naam = naam;
        }

        public string Hoofdstad { get { return _hoofdstad; } set { _hoofdstad = value; } }
        public string Naam { get { return _naam; } set { _naam = value; } }

        public override string ToString()
        {
            return Convert.ToString(this.GetHashCode());
        }
        public override bool Equals(object? obj)
        {
            bool resultaat = false;

            if (obj != null)
            {
                if (GetType() == obj.GetType())
                {
                    Land r = (Land)obj;
                    if (this.Naam == r.Naam && this.Hoofdstad == r.Hoofdstad)
                    {
                        resultaat = true;
                    }
                }
            }
            return resultaat;
        }

    }
}
